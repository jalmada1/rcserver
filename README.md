# RCServer

This is what worked for me when creating a Raspberry Pi zero with IR transmiter/Receiver. This is a compilation of many different sources I used and the parts that worked for me in each one.

# Disclaimer 

This doc is mostly for future me, but if somebody else happens to stumble with this, take everything with a huge grain of salt, I have no idea what I am doing most of the time and if I explain something it is how I understand it.

# Hardware
- IR sensor (adafruit [link](http://www.adafruit.com/products/157))
- IR LED (adafruit [link](http://www.adafruit.com/products/387))
- Arduino or Raspberry Pi
- NPN Transistor PN2222
- 1K ohm resistors
- 220 ohm resistor
- Some connecting wires

# Receiver setup

## Modify os files.
> Note: **DO NOT** do anything related to `/etc/modules` or `/etc/lirc/hardware.conf` those modifications are not needed anymore.

`sudo nano /boot/config.txt`
Modify this section (like dont uncomment, replace the whole section with this): 

```bash
# Uncomment this to enable infrared communication.
dtoverlay=gpio-ir,gpio_pin=18
dtoverlay=gpio-ir-tx,gpio_pin=17
```

## Lirc Setup

1. Install lirc: `sudo apt-get install lirc`

2. Modify: `/etc/lirc/lirc_options.conf`
Replace with:

```bash
driver          = default
device          = /dev/lirc0
output          = /var/run/lirc/lirc0
pidfile         = /var/run/lirc/lirc0.pid
```
3. Reboot: `sudo reboot`

4. Check that configuration changes worked:
`ls -l /dev/lirc0` should give you: `crw-rw---- 1 root video 251, 0 Sep 26 00:17 /dev/lirc0` (or something similar)

5. Check services:

- `systemctl status lircd.service`
- `systemctl status lircd.socket`

> Note: I'm not 100% sure why there is a socket and a service and at what point they were added to the linux services, so Im assuming lirc does that based on the `lirc_options.conf` previously modified, anyway these are important (socket maybe not????)

> About services: Service definitions can be found in `/lib/systemd/system` or `/etc/systemd/system`, just look for the file with the same name as the service (in this case lircd.service).

6. Test

First you need to stop the service and socket related to the device used for receiving signals (lirc0).

- `sudo systemctl stop lircd.service`
- `sudo systemctl stop lircd.socket`

Then you need to run mode2 to see if your hardware configuration works:

- `sudo mode2 --driver default --device /dev/lirc0`

If you see a bunch of numbers when you point the rc to your receiver and click any button, then you are good.

7. Add Remote Controller configuration file.

In the repo I have the rc I added to my raspi3 and zero, but you can find a lot more preconfigured files [here](http://lirc-remotes.sourceforge.net/remotes-table.html).
- Copy the file `lgac.lircd.config` to `/etc/lirc/lircd.conf.d/`
- Rename `devinput.lircd.conf` (in that same folder) to `devinput.lircd.conf.dist`

> Note: I created this rc config file myself, right now I have nothing here on how to do this, but you can check some of the source links to get an idea and how to setup the hardware and create your own.

8. Test Emitter.

In the pictures you can see Im using a led for testing, but phone cameras can see infrared so you can also try that, just consider that the emited light can be very faint (like very very faint, you really need to focus sometimes to see anything, that is why the led)

To test you need to run this command:
- `irsend SEND_START LGAC2 ONOFF`
This will constantly send a pulse that would turn on or off the device, to stop:
- `irsend SEND_STOP LGAC2 ONOFF`
To send a single pulse you can use:
- `irsend SEND_ONCE LGAC2 ONOFF`

The command line structure is this:
- irrsend: the tool that sends the command to the device.
- SEND_ONCE: how this will be send (check the docs for irsend for more details: [here](https://www.lirc.org/html/irsend.html).)
- LGAC2: the name of the device specified in the `name` property inside `lgac.lircd.config` file.
- ONOFF: the command name, also you can check the available ones inside the `lgac.lircd.config` file.

## The web server

The source code contains a little flask app (like really little 20 lines tops) that excecutes internally these commands, plese modify 

## Sources

- Software: https://raspberrypi.stackexchange.com/questions/81876/raspberry-pi-3-not-lirc-not-running-working
- Hardware: https://www.instructables.com/How-To-Useemulate-remotes-with-Arduino-and-Raspber/
- Lirc: https://www.lirc.org/html/configuration-guide.html
- Deploy flask to prod: https://flask.palletsprojects.com/en/2.0.x/tutorial/deploy/
- Create a service in linux: https://www.shubhamdipt.com/blog/how-to-create-a-systemd-service-in-linux/
