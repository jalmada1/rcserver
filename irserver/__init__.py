from flask import Flask, jsonify, render_template, make_response
from flask_cors import CORS
import subprocess
from markupsafe import escape
import os

def create_app():
        app = Flask(__name__)
        CORS(app)

        @app.route("/")
        def index():
                return render_template('index.html')

        @app.route("/<rc>/<code>")
        def irsend(rc, code):
                try:
                        rtn = subprocess.call(["irsend", "SEND_ONCE", escape(rc), escape(code)])

                        return jsonify(rc = rc,
                                code = code,
                                message= f"Called {escape(code)} on {escape(rc)}")
                except Exception as error:
                        resp = make_response("Server Error", 500)
                        return resp

        return app

if __name__ == '__main__':
        app = create_app()
        app.run(port=5000)