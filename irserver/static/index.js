function runCommand(source, rc){
    var command = source.id;
    var rc = document.getElementById('rc').value;
    var response = axios.get("/" + rc +"/" + command)
    .then(response => {
        document.getElementById("status").innerHTML = response.data.message;
        console.log(response);
    })
    .catch(error => {
        document.getElementById("status").innerHTML = error;
    });
}